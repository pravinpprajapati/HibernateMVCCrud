package com.hibernate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HibernateMvcCrudApplicationTests {

	@Test
	public void passTest() {
	    String testPass = "Pass";
	    assert (testPass.equals("Pass"));
	}
	
	@Test
	public void failTest() {
		String testPass = "Pass";
		assert (testPass.equals("Fail"));
	}

}
